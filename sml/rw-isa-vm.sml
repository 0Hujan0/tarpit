structure Machine = struct
  datatype instruction = HLT | OUT | BPL | SUB | IN | MOV | BEQ | ADDP | UNKNOWN
  type state = {memory:Word8.word Array.array, pc:(int ref), revision:int, ptrsize:int}

  fun memory{memory=memory, pc=_, revision=_, ptrsize=_} = memory
  fun pc{memory=_, pc=pc, revision=_, ptrsize=_} = pc
  fun revision{memory=_, pc=_, revision=revision, ptrsize=_} = revision
  fun ptrsize{memory=_, pc=_, revision=_, ptrsize=ptrsize} = ptrsize
  fun newMachine(data) : state = {memory=data, pc=ref 0, revision=0, ptrsize=4}
  fun incrementPC(machine) = pc machine := !(pc machine) + 1
  fun setPC(machine, newPc) = pc machine := newPc

  fun getByteAt(machine, i) =
    if i < Array.length (memory machine) andalso i >= 0
    then
        Array.sub(memory machine, i)
    else raise Fail ("Could not read memory address: " ^ (Int.toString i))

  fun getCurrentByte(machine) = getByteAt(machine, !(pc machine))

  fun putByteAt (machine, ptr, byte) = (Array.update(memory machine, ptr, byte); machine)

  fun instructionFromByte(word, revision) =
    let val value = Word8.toInt(word) in
    case value of
         0 => HLT
       | 1 => OUT
       | 2 => BPL
       | 3 => SUB
       | 4 => IN
       | other => if revision = 2
         then  case other of
            5 => MOV
          | 6 => BEQ
          | 7 => ADDP
          | _ => UNKNOWN
         else UNKNOWN
    end


  fun pow (0, power) = 0
    | pow (base, 0) = 1
    | pow (base, power) = base * pow(base, power -1)

  fun pointerFromBytes(machine, 0) = (machine, 0)
    | pointerFromBytes(machine, i) =
    let
      val byte = getCurrentByte(machine)
      val (nextMachine, nextVal) = (incrementPC machine; pointerFromBytes (machine, i-1))
    in
       (nextMachine, ((nextVal) * (pow (2, 8)) ) + Word8.toInt(byte))
    end

  fun getPointer(machine) = 
    let
      val (_, pointer) = pointerFromBytes(machine, ptrsize machine)
    in
      pointer
    end

  fun byteFromPtr (ptr, pos) =
  let
    val mask = IntInf.<<(IntInf.fromInt(255), Word.fromInt (8 * pos))
    val byte = IntInf.toInt (IntInf.~>>((IntInf.andb (IntInf.fromInt ptr, mask)), Word.fromInt (8 * pos)))
  in
    Word8.fromInt(byte)
  end

  fun writePointer(machine, dstptr, ptrvalue) =
  let
    val bytes = Array.tabulate(ptrsize machine, fn i => byteFromPtr(ptrvalue,  i))
  in
    Array.copy{src=bytes, dst=(memory machine), di=dstptr}
  end


  fun output(machine) =
    let
      val pointer = getPointer machine
      val byte = getByteAt (machine, pointer)
      val slice = Word8VectorSlice.slice (Word8Vector.fromList([byte]), 0, SOME(1))
    in
      Posix.IO.writeVec(Posix.FileSys.stdout, slice); machine
    end

  fun branchIf(machine) =
    let
      val jmpptr = getPointer machine
      val srcptr = getPointer machine
      val byte = getByteAt (machine, srcptr)
    in
      if (Word8.toInt byte) < 128 then (setPC(machine, jmpptr); machine) else machine
    end

  fun subtract(machine) =
    let
      val dstptr = getPointer machine
      val srcptr = getPointer machine
      val dstbyte = getByteAt (machine, dstptr)
      val srcbyte = getByteAt (machine, srcptr)
    in
      putByteAt (machine, dstptr, dstbyte - srcbyte)
    end

  fun input(machine) =
    let
      val dstptr = getPointer machine
      val bytes = Posix.IO.readVec(Posix.FileSys.stdin, 1)
    in
      if Word8Vector.length bytes > 0
      then putByteAt (machine, dstptr, Word8Vector.sub (bytes,0))
      else putByteAt (machine, dstptr, Word8.fromInt 0)
    end

  fun move(machine) =
  let
      val dstptr = getPointer machine
      val srcptr = getPointer machine
  in
      putByteAt (machine, dstptr, getByteAt(machine, srcptr))
  end

  fun branchIfZero(machine) =
    let
      val jmpptr = getPointer machine
      val srcptr = getPointer machine
      val byte = getByteAt (machine, srcptr)
    in
      if (Word8.toInt byte) = 0 then (setPC(machine, jmpptr); machine) else machine
    end

  fun addPointers(machine) =
    let
      val dstptr = getPointer machine
      val srcptr = getPointer machine
      val pc = !(pc machine)
      val _ = setPC (machine, dstptr)
      val dstval = getPointer machine
      val _ = setPC (machine, srcptr)
      val srcval = getPointer machine
    in
        (writePointer(machine, dstptr, srcval + dstval); setPC (machine, pc); machine)
    end


  fun execute(machine) =
    let
      val memory = instructionFromByte(getCurrentByte machine, revision machine)
    in
      case memory of
           HLT => OS.Process.exit OS.Process.success
         | OUT => (incrementPC machine; execute (output machine))
         | BPL => (incrementPC machine; execute (branchIf machine))
         | SUB => (incrementPC machine; execute (subtract machine))
         | IN => (incrementPC machine; execute (input machine))
         | MOV => (incrementPC machine; execute (move machine))
         | BEQ => (incrementPC machine; execute (branchIfZero machine))
         | ADDP => (incrementPC machine; execute (addPointers machine))
         | UNKNOWN => (print "Unknown instruction at position:";
           print (Int.toString (!(pc machine)));
           print "\n Instruction:";
           print (Word8.toString (getCurrentByte machine));
           OS.Process.exit OS.Process.failure)
    end

  fun parseHeader(machine: state) =
    if Array.length (memory machine) < 4
    then machine
    else if not (Byte.byteToChar (getByteAt(machine, 0)) = #"R" andalso Byte.byteToChar (getByteAt(machine, 1)) = #"W")
    then machine
    else
      let
        val _ = setPC(machine, 2)
        val revision = Word8.toInt(getCurrentByte machine) - 97
        val _ = incrementPC machine
        val ps = pow(2, Word8.toInt(getCurrentByte machine) - 48)
        val _ = incrementPC machine;
        val (machine, eof) = pointerFromBytes(machine, ps)
        val (machine, eom) = pointerFromBytes(machine, ps)
        val mem = Array.array(eom, Word8.fromInt 0)
        val text = ArraySlice.slice (memory machine, 0, SOME(eof))
      in
        (ArraySlice.copy {src=text, dst=mem, di=0};
        {memory=mem, pc=(pc machine), revision=revision, ptrsize=ps})
      end
end

fun machineFromFile(filename) =
    let
      val file = BinIO.openIn filename
      val vector = BinIO.inputAll file
      val array = Array.tabulate (Word8Vector.length vector, fn i => Word8Vector.sub (vector, i))
    in
      Machine.parseHeader (Machine.newMachine array)
    end

fun run(filename: string) = Machine.execute(machineFromFile filename)

fun runAll(filename::tail) = ( run filename; runAll tail)
  | runAll([]) = ()

val args = CommandLine.arguments()
val _ = runAll args
val _ = OS.Process.exit(OS.Process.success)
