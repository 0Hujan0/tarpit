#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "configuration.h"
#include "util.h"
#include "data_structures.h"

#define BUFFER_SIZE 1024
void usage(const char* name)
{
    printf("%s [ps={0,1,2,3}] [is={0,1,2}] <input> <output>\n", name);
}

int main( int argc, char* argv[])
{
    if (argc < 2)
    {
        usage(argv[0]);
        exit(1);
    }
    config c = parse_configuration(argc, argv);
    print_config(c);

    FILE* input_file = fopen(c.in_filename, "r");
    if ( input_file == NULL )
    {
        error("Could not open input file.\n");
    }
    FILE* output_file;
    if (c.out_filename == NULL)
    {
        char filename[] = "out.rw??";
        filename[6] = c.instruction_set + 'a';
        filename[7] = c.pointer_size + '0';
        output_file = fopen(filename, "wb");
    }
    else
    {
        output_file = fopen(c.out_filename, "wb");
    }
    if ( output_file == NULL )
    {
        fclose(input_file);
        error("Could not open output file.\n");
    }

    char buffer[BUFFER_SIZE];
    memset(buffer, 0, 1024);
    unsigned char pointer_size = uchar_pow(2, c.pointer_size);
    printf("Actual pointer size: %i\n", (int)pointer_size);
    instruction_list instructions = {.head = NULL, .tail = NULL};
    byte_list data = {.head = NULL, .tail = NULL};

    pointer_type text_segment_length = init_pointer_with_byte(0, pointer_size);
    pointer_type data_segment_length = init_pointer_with_byte(0, pointer_size);
    pointer_type bss_segment_length = init_pointer_with_byte(0, pointer_size);

    int is_comment = 0;

    for( int is_comment=0, read=0, buf_index = 0;
            (read = fgetc(input_file)) != EOF;
            buf_index++ )
    {
        if ( buf_index >= BUFFER_SIZE )
        {
            printf("Buffer index %d out of range. Buffer size: %d", buf_index, BUFFER_SIZE);
            exit(1);
        }
        char read_char = (char) read;

        if ( is_comment )
        {
            is_comment = read_char == '\n' ? 0 : 1 ;
            for ( ;buf_index >= 0; buf_index-- )
            {
                buffer[buf_index] = 0;
            }
            continue;
        }
        else if ( read_char == ';' )
        {
            is_comment = 1;
        }

        if ( read_char != ' ' && read_char != '\n' && read_char != '\t' )
        {
            buffer[buf_index] = read_char;
            continue;
        }

        int inst_size = 0;
        if ( strcmp(buffer, "HLT") == 0 )
        {
            instruction inst = {.instruction = 0, .argument_count = 0};
            insert_instruction(&instructions, inst);
            inst_size = 1;
        }
        else if ( strcmp(buffer, "OUT") == 0 )
        {
            instruction inst = {.instruction = 1, .argument_count = 1};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + pointer_size;
        }
        else if ( strcmp(buffer, "BPL") == 0 )
        {
            instruction inst = {.instruction = 2, .argument_count = 2};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            inst.arguments[1] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + 2 * pointer_size;
        }
        else if ( strcmp(buffer, "SUB") == 0 )
        {
            instruction inst = {.instruction = 3, .argument_count = 2};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            inst.arguments[1] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + 2 * pointer_size;
        }
        else if ( strcmp(buffer, "IN") == 0 )
        {
            instruction inst = {.instruction = 4, .argument_count = 1};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + pointer_size;
        }
        else if ( strcmp(buffer, "MOV") == 0 )
        {
            instruction inst = {.instruction = 5, .argument_count = 2};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            inst.arguments[1] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + 2 * pointer_size;
        }
        else if ( strcmp(buffer, "BEQ") == 0 )
        {
            instruction inst = {.instruction = 6, .argument_count = 2};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            inst.arguments[1] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + 2 * pointer_size;
        }
        else if ( strcmp(buffer, "ADD.P") == 0 )
        {
            instruction inst = {.instruction = 7, .argument_count = 2};
            inst.arguments[0] = parse_pointer(input_file, pointer_size, 1);
            inst.arguments[1] = parse_pointer(input_file, pointer_size, 1);
            insert_instruction(&instructions, inst);
            inst_size = 1 + 2 * pointer_size;
        }
        else if ( strcmp(buffer, "bss:") == 0 )
        {
            destroy_pointer_type(bss_segment_length);
            bss_segment_length = parse_pointer(input_file, pointer_size, 1);
        }
        else if ( strcmp(buffer, "data:") == 0 )
        {
            char buff;
            while ( parse_bytes(input_file, &buff, 1, 0))
            {
                insert_byte(&data, buff);
                add_byte_to_pointer(data_segment_length, 1);
            }
        }

        if (inst_size)
        {
            add_byte_to_pointer(text_segment_length, inst_size);
        }

        for ( ;buf_index >= 0; buf_index-- )
        {
            buffer[buf_index] = 0;
        }
    }
    pointer_type header_size;
    pointer_type eof;
    pointer_type eom;
    int include_bss = 0;
    if ( c.instruction_set > 0 )
    {
        header_size = init_pointer_with_byte(4 + 2 * pointer_size, pointer_size);
        eof = copy_pointer(text_segment_length);
        if (!add_pointers(eof, data_segment_length) || !add_pointers(eof, header_size))
        {
            error("eof (header+text+data) exceeds addressable size!\n");
        }
        eom = copy_pointer(eof);
        if (!add_pointers(eom, bss_segment_length))
        {
            printf("eom (eof+bss) exceeds pointer size. Setting eom to maximum addressable size.\n");
            set_pointer_to_max(eom);
        }

        header h = create_header(c.instruction_set, c.pointer_size, eof, eom);
        fwrite(&h, 4, 1, output_file);
        fwrite(h.eof.pointer, h.eof.pointer_size, 1, output_file);
        fwrite(h.eom.pointer, h.eom.pointer_size, 1, output_file);
    }
    else
    {
        header_size = init_pointer_with_byte(0, pointer_size);
        include_bss = 1;
        eof = copy_pointer(text_segment_length);
        if(!add_pointers(eof, data_segment_length))
        {
            error("eof (text+data) exceeds addressable size!\n");
        }
        eom = copy_pointer(eof);
        if(!add_pointers(eom, bss_segment_length))
        {
            printf("eom (eof+bss) exceeds addressable size. Setting eom to maximum addressable size.\n");
            set_pointer_to_max(eom);
            set_pointer_to_max(bss_segment_length);
            subtract_pointers(bss_segment_length, eof);
        }
    }

    pointer_type current_address = copy_pointer(header_size);
    add_pointers(text_segment_length, header_size);

    for (instruction_list_node* node = instructions.head; node != NULL; node = node->next)
    {
        unsigned char instruction_buffer[1 + 2 * pointer_size];
        unsigned char inst_buff_size = 1;
        instruction inst = node->instruction;
        instruction_buffer[0] = inst.instruction;

        add_byte_to_pointer(current_address, 1);
        for (int i = 0; i < inst.argument_count; i++)
        {
            int subtract = 0;
            pointer_type offset;
            if ( inst.arguments[i].section == NULL )
            {
                offset = init_pointer_with_byte(0, pointer_size);
            }
            else if ( strcmp(inst.arguments[i].section, "data") == 0 )
            {
                offset = copy_pointer(text_segment_length);
            }
            else if ( strcmp(inst.arguments[i].section, "bss") == 0 )
            {
                offset = copy_pointer(eof);
            }
            else if ( strcmp(inst.arguments[i].section, "text") == 0 )
            {
                offset = copy_pointer(header_size);
            }
            else if ( strcmp(inst.arguments[i].section, "@" ) == 0 )
            {
                offset = copy_pointer(current_address);
            }
            else if ( strcmp(inst.arguments[i].section, "@-" ) == 0 )
            {
                subtract = 1;
                offset = copy_pointer(current_address);
            }
            else
            {
                offset = init_pointer_with_byte(0, pointer_size);
            }

            if ( subtract )
            {
                subtract_pointers(offset, inst.arguments[i]);
                inst.arguments[i] = offset;
            }
            else
            {
                add_pointers(inst.arguments[i], offset);
            }

            if ( compare_pointers(inst.arguments[i], eom) >= 0 )
            {
                error("Address reference out of bounds.\n");
            }
            for (int j = 0; j < inst.arguments[i].pointer_size; j++)
            {
                instruction_buffer[ inst_buff_size + j] = inst.arguments[i].pointer[j];
            }
            inst_buff_size += inst.arguments[i].pointer_size;
            add_byte_to_pointer(current_address, inst.arguments[i].pointer_size);
            destroy_pointer_type(offset);
        }
        fwrite(instruction_buffer, inst_buff_size, 1, output_file);
    }
    for (byte_list_node* node = data.head; node != NULL; node = node->next)
    {
        fwrite(&(node->byte), 1, 1, output_file);
    }
    if (include_bss)
    {
        char zero = 0;
        pointer_type it = init_pointer_with_byte(0, pointer_size);
        for (; compare_pointers(it, bss_segment_length) < 0; add_byte_to_pointer(it, 1))
        {
            fwrite(&zero, 1, 1, output_file);
        }
    }
    destroy_instruction_list(instructions);
    destroy_byte_list(data);
    destroy_pointer_type(text_segment_length);
    destroy_pointer_type(data_segment_length);
    destroy_pointer_type(bss_segment_length);
    destroy_pointer_type(header_size);
    destroy_pointer_type(current_address);
    destroy_pointer_type(eof);
    destroy_pointer_type(eom);

    fclose(input_file);
    fclose(output_file);

}
