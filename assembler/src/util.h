#ifndef ASM_UTIL_H
#define ASM_UTIL_H
#include <stdio.h>
#include "data_structures.h"
void error(const char* message);
unsigned char uchar_pow(unsigned char base, unsigned char exponent);
int parse_bytes(FILE* file, char* buffer, unsigned char num_bytes, int error_on_eof);
int add_byte_to_pointer(pointer_type pointer, unsigned char byte);
int add_pointers(pointer_type first, pointer_type second);
int subtract_pointers(pointer_type first, pointer_type second);
pointer_type init_pointer_with_byte(unsigned char initial_value, unsigned char pointer_size);
pointer_type copy_pointer(pointer_type pointer);
void set_pointer_to_max(pointer_type pointer);
int compare_pointers(pointer_type first, pointer_type second);
void print_pointer(pointer_type pointer);
pointer_type parse_pointer(FILE* file, unsigned char pointer_size, int error_on_eof);
pointer_type parse_address(const char* buffer, unsigned char pointer_size);
#endif
