#include "util.h"
#include "data_structures.h"
#include <stdio.h>
#include <stdlib.h>
void error(const char* message)
{
    printf("%s", message);
    exit(1);
}

unsigned char uchar_pow(unsigned char base, unsigned char exponent)
{
    unsigned char return_value = 1;
    for (; exponent > 0; exponent--)
    {
        return_value *= base;
    }

    return return_value;
}

int get_nibble_from_hex(char character, unsigned char* nibble)
{
    char value = character -'0';
    if (value > 9)
    {
        value = 10 + character - 'a';
        if (value > 15 || value < 10)
        {
            value = 10 + character - 'A';
            if (value > 15 || value < 10)
            {
                return 0;
            }
        }
    }
    *nibble = value;
    return 1;
}

int parse_bytes(FILE* file, char* buffer, unsigned char num_bytes, int error_on_eof)
{
    unsigned char value;
    int read;
    char c;

    for (int i = 0; i < num_bytes; i++)
    {
        // Parse first nibble
        do
        {
            read = fgetc(file);
            if (read == EOF)
            {
                if (error_on_eof)
                {
                    error("Reached end of file while parsing byte data.\n");
                }
                else
                {
                    return 0;
                }
            }
        } while( read == '\n' || read == ' ' );

        c = (char) read;
        if (c == '\'')
        {
            read = fgetc(file);
            if (read == EOF)
            {
                error("Reached end of file while parsing character in single quote.\n");
            }
            c = (char) read;
            if (c == '\\')
            {
                read = fgetc(file);
                if (read == EOF)
                {
                    error("Reached end of file while parsing character in single quote.\n");
                }
                c = (char) read;
                switch (c)
                {
                    case '0':
                        c = '\0';
                        break;
                    case 'n':
                        c = '\n';
                        break;
                    case '\\':
                        c = '\\';
                        break;
                    default:
                        error("Escaped character not recognised.\n");
                }
            }
            buffer[num_bytes - 1 - i] = c;
            read = fgetc(file);
            if (read == EOF)
            {
                error("Reached end of file while parsing character in single quote.\n");
            }
            c = (char) read;
            if (c != '\'')
            {
                error("Reached end of file while parsing character in single quote.\n");
            }
            continue;
        }

        if ( ! get_nibble_from_hex(c, &value) )
        {
            error("Unable to parse byte value.\n");
        }

        unsigned char byte_value = value;
        // Parse second nibble
        read = fgetc(file);
        if (read == EOF)
        {
            error("Reached end of file in the middle of parsing a byte.\n");
        }
        c = (char) read;

        if ( ! get_nibble_from_hex(c, &value) )
        {
            error("Unable to parse byte value.\n");
        }

        byte_value *= 16;
        byte_value += value;
        buffer[num_bytes - 1 - i] = byte_value;
    }
    return 1;
}

int add_byte_to_pointer(pointer_type pointer, unsigned char byte)
{
    pointer_type byte_pointer = init_pointer_with_byte(byte, pointer.pointer_size);
    int carry = add_pointers(pointer, byte_pointer);
    destroy_pointer_type(byte_pointer);
    return carry;
}

int add_pointers(pointer_type first, pointer_type second)
{
    unsigned char carry = 0;
    for (unsigned char i = 0; i < first.pointer_size; i++)
    {
        unsigned char first_val = first.pointer[i];
        unsigned char second_val = i < second.pointer_size ? second.pointer[i] : 0;
        first.pointer[i] = first_val + second_val + carry;
        if ( first.pointer[i] < first_val || first.pointer[i] < second_val || (first.pointer[i] == first_val && carry == 1) )
        {
            carry = 1;
        }
        else
        {
            carry = 0;
        }
    }
    return !carry;
}

int subtract_pointers(pointer_type first, pointer_type second)
{
    unsigned char borrow = 0;
    for (unsigned char i = 0; i < first.pointer_size; i++)
    {
        unsigned char first_val = first.pointer[i];
        unsigned char second_val = i < second.pointer_size ? second.pointer[i] : 0;

        first.pointer[i] = first_val - second_val - borrow;
        if ( first.pointer[i] > first_val || (first.pointer[i] == first_val && borrow==1) )
        {
            borrow = 1;
        }
        else
        {
            borrow = 0;
        }
    }
    return !borrow;
}

pointer_type init_pointer_with_byte(unsigned char initial_value, unsigned char pointer_size)
{
    pointer_type pointer = { .pointer=NULL, .pointer_size=pointer_size, .section=NULL };
    if ( pointer_size == 0 )
    {
        return pointer;
    }
    pointer.pointer = malloc(sizeof(unsigned char) * pointer_size);
    pointer.pointer[0] = initial_value;
    for (unsigned char i = 1; i < pointer.pointer_size; i++)
    {
        pointer.pointer[i] = 0;
    }
    return pointer;
}

pointer_type copy_pointer(pointer_type pointer)
{
    pointer_type ret_pointer = init_pointer_with_byte(0, pointer.pointer_size) ;
    for (unsigned char i = 0; i < pointer.pointer_size; i++)
    {
        ret_pointer.pointer[i] = pointer.pointer[i];
    }
    return ret_pointer;
}

void set_pointer_to_max(pointer_type pointer)
{
    for (unsigned char i = 0; i < pointer.pointer_size; i++)
    {
        pointer.pointer[i] = 0xFF;
    }
}
int compare_pointers(pointer_type first, pointer_type second)
{
    int value = 0;
    for (unsigned char i = 0; i < first.pointer_size; i++)
    {
        unsigned char second_val = i < second.pointer_size ? second.pointer[i] : 0;
        if (first.pointer[i] != second_val)
        {
            value = first.pointer[i] < second_val ? -1 : 1;
        }
    }
    return value;
}
void print_pointer(pointer_type pointer)
{
    for (char i = pointer.pointer_size - 1; i >= 0; i--)
    {
        char first_nibble = pointer.pointer[i] / 16;
        if (first_nibble < 10)
        {
            first_nibble += '0';
        }
        else
        {
            first_nibble += 'A' - 10;
        }
        char second_nibble = pointer.pointer[i] % 16;
        if (second_nibble < 10)
        {
            second_nibble += '0';
        }
        else
        {
            second_nibble += 'A' - 10;
        }
        printf("%c%c", first_nibble, second_nibble);
    }
}

pointer_type parse_pointer(FILE* file, unsigned char pointer_size, int error_on_eof)
{
    pointer_type pointer;
    char buffer[1024];
    if(fscanf(file, "%s", buffer) > 0)
    {
        pointer = parse_address(buffer, pointer_size);
    }
    else
    {
        pointer = init_pointer_with_byte(0, pointer_size);
    }
    //parse_bytes(file, pointer.pointer, pointer_size, error_on_eof);
    return pointer;
}

int set_nibble(pointer_type pointer, unsigned char nibble_value, unsigned char nibble_offset)
{
    if ( pointer.pointer_size * 2 <= nibble_offset )
    {
        return 0;
    }
    unsigned char offset = nibble_offset / 2;
    unsigned char original_byte = pointer.pointer[offset];

    if ( nibble_offset % 2 == 0 )
    {
        original_byte &= 0x0F;
        original_byte += nibble_value * 16;
    }
    else
    {
        original_byte &= 0xF0;
        original_byte += nibble_value;
    }
    pointer.pointer[offset] = original_byte;
    return 1;
}

// TODO: Make sure single nibbles are not accidentilly multiplied by 16
// TODO: Add way to define address as both multiples of pointer size and actual bytes
pointer_type parse_address(const char* buffer, unsigned char pointer_size)
{
    pointer_type pointer = init_pointer_with_byte(0, pointer_size);

    int i=0;

    if (buffer[i] =='#')
    {
        pointer.section = NULL;
    }
    else if (buffer[i] =='@' && buffer[i+1] == '-')
    {
        pointer.section = malloc(3 * sizeof(char));
        pointer.section[0] = '@';
        pointer.section[1] = '-';
        pointer.section[2] = '\0';
        i++;
    }
    else if (buffer[i] =='@')
    {
        pointer.section = malloc(2 * sizeof(char));
        pointer.section[0] = '@';
        pointer.section[1] = '\0';
    }
    else
    {
        // Parse section
        for (i = 0; buffer[i] != '[' && buffer[i] != '\0'; i++)
        { }

        if ( i > 0 )
        {
            pointer.section = malloc( (i+1) * sizeof(char));
            for (int j=0; j<i; j++)
            {
                pointer.section[j] = buffer[j];
            }
            pointer.section[i] = '\0';
        }
    }

    if (buffer[i] == '\0')
    {
        return pointer;
    }

    i++;

    int nibble_offset = 0;
    for (; buffer[i] != ']' && buffer[i] != '\0'; i++)
    {
        if (buffer[i] == '\n' || buffer[i] == ' ')
        {
            continue;
        }

        unsigned char value;
        if ( ! get_nibble_from_hex(buffer[i], &value) )
        {
            printf("Nibble: %c\n", buffer[i]);
            error("Unable to parse nibble value.\n");
        }
        if (!set_nibble(pointer, value, nibble_offset))
        {
            error("Pointer value does not fit into pointer size.\n");
        }
        nibble_offset +=1;
    }
    return pointer;
}
