#include "data_structures.h"
#include "util.h"
#include <stdlib.h>

void insert_instruction(instruction_list* list, instruction inst)
{
    instruction_list_node* node = malloc(sizeof(instruction_list_node));
    if (node == NULL)
    {
        error("Could not allocate memory.\n");
    }
    node->instruction = inst;
    node->next = NULL;
    if (list->head == NULL && list->tail == NULL)
    {
        list->head = node;
        list->tail = node;
        return;
    }
    else if (list->head == NULL || list->tail == NULL)
    {
        error("Instruction list in illegal state.\n");
    }
    list->tail->next = node;
    list->tail = node;
}

void destroy_instruction_list(instruction_list list)
{
    instruction_list_node* node = list.head;
    for (; node != list.tail; )
    {
        instruction_list_node* temp = node;
        node = node->next;
        destroy_instruction(temp->instruction);
        free (temp);
    }
    if (node)
    {
        destroy_instruction(node->instruction);
        free (node);
    }
}
void destroy_instruction(instruction inst)
{
    for (int i=0; i<inst.argument_count; i++)
    {
        destroy_pointer_type(inst.arguments[i]);
    }
}

void destroy_pointer_type(pointer_type pointer)
{
    free(pointer.pointer);
    free(pointer.section);
}

void insert_byte(byte_list* list, unsigned char byte)
{
    byte_list_node* node = malloc(sizeof(byte_list_node));
    if (node == NULL)
    {
        error("Could not allocate memory.\n");
    }
    node->byte = byte;
    node->next = NULL;
    if (list->head == NULL && list->tail == NULL)
    {
        list->head = node;
        list->tail = node;
        return;
    }
    else if (list->head == NULL || list->tail == NULL)
    {
        error("Byte list in illegal state.\n");
    }
    list->tail->next = node;
    list->tail = node;
}

void destroy_byte_list(byte_list list)
{
    byte_list_node* node = list.head;
    for (; node != list.tail; )
    {
        byte_list_node* temp = node;
        node = node->next;
        free (temp);
    }
    if (node)
    {
        free (node);
    }
}

header create_header(unsigned char revision, unsigned char ptr_size, pointer_type eof, pointer_type eom)
{
    header h = {
        .magic_1 = 'R',
        .magic_2 = 'W',
        .isa_revision=revision+'a',
        .ptr_size=ptr_size+'0',
        .size=4 + eom.pointer_size + eof.pointer_size};
    h.eof = eof;
    h.eom = eom;
    return h;
}
