#ifndef ASM_DATA_STRUCTURES_H
#define ASM_DATA_STRUCTURES_H
typedef struct
{
    unsigned char* pointer;
    unsigned char pointer_size;
    char* section;
} pointer_type;

void destroy_pointer_type(pointer_type pointer);

typedef struct
{
    unsigned char instruction;
    pointer_type arguments[2];
    unsigned char argument_count;
} instruction;

typedef struct _inst_list_node
{
    instruction instruction;
    struct _inst_list_node* next;
} instruction_list_node;

typedef struct
{
    instruction_list_node* head;
    instruction_list_node* tail;

} instruction_list;

void insert_instruction(instruction_list* list, instruction inst);
void destroy_instruction_list(instruction_list list);
void destroy_instruction(instruction inst);

typedef struct _byte_list_node
{
    unsigned char byte;
    struct _byte_list_node* next;
} byte_list_node;

typedef struct
{
    byte_list_node* head;
    byte_list_node* tail;

} byte_list;

void insert_byte(byte_list* list, unsigned char byte);
void destroy_byte_list(byte_list list);

typedef struct
{
    char magic_1;
    char magic_2;
    unsigned char isa_revision;
    unsigned char ptr_size;
    pointer_type eof;
    pointer_type eom;
    unsigned char size;
} header;

header create_header(unsigned char revision, unsigned char ptr_size, pointer_type eof, pointer_type eom);

#endif
