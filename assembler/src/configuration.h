#ifndef ASM_CONFIGURATION_H
#define ASM_CONFIGURATION_H
typedef struct configuration
{
    char pointer_size;
    char instruction_set;
    const char* in_filename;
    const char* out_filename;
} config;

void print_config(config c);
config parse_configuration(int argc, char **argv);
#endif
