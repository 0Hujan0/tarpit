#include <stdlib.h>
#include <stdio.h>
#include "configuration.h"
#include "util.h"

void print_config(config c)
{
    char output[9] = "out.rw??";
    output[6] = 'a' + c.instruction_set;
    output[7] = '0' + c.pointer_size;
    const char* output_ptr = output;

    if(c.out_filename != NULL)
    {
        output_ptr = c.out_filename;
    }

    printf("ps=%i, is=%i, input=%s, output=%s\n", c.pointer_size, c.instruction_set, c.in_filename, output_ptr);
}

config parse_configuration(int argc, char **argv)
{
    config configuration;
    configuration.pointer_size = -1;
    configuration.instruction_set = -1;
    configuration.in_filename = NULL;
    configuration.out_filename = NULL;
    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == 'p'
            && argv[i][1] == 's'
            && argv[i][2] == '=')
        {
            if ( configuration.pointer_size != -1)
            {
                error("ps argument provided more than once\n");
            }
            if ( argv[i][3] >= '0' && argv[i][3] <= '3' )
            {
                configuration.pointer_size = argv[i][3] - '0';
            }
            else
            {
                error("ps value not supported. Supported values: 0, 1, 2, 3\n");
            }
            continue;
        }

        if (argv[i][0] == 'i'
            && argv[i][1] == 's'
            && argv[i][2] == '=')
        {
            if ( configuration.instruction_set != -1)
            {
                error("is argument provided more than once\n");
            }
            if ( argv[i][3] >= '0' && argv[i][3] <= '2' )
            {
                 configuration.instruction_set = argv[i][3] - '0';
            }
            else
            {
                error("is value not supported. Supported values: 0, 1, 2\n");
            }
            continue;
        }

        if(configuration.in_filename == NULL)
        {
            configuration.in_filename = argv[i];
            continue;
        }

        if(configuration.out_filename == NULL)
        {
            configuration.out_filename = argv[i];
            continue;
        }
        error("Two many command line arguments provided.\n");
    }
    if (configuration.in_filename == NULL)
    {
        error("No input filename provided\n");
    }
    configuration.pointer_size = configuration.pointer_size == -1 ? 2 : configuration.pointer_size;
    configuration.instruction_set = configuration.instruction_set == -1 ? 0 : configuration.instruction_set;

    return configuration;
}
