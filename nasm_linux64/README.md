NASM rwisa-vm
=============

This is an implementation of a VM for the RW instruction set in the Netwide Assembler (NASM).
It is written for x86\_64 processors and uses 64 bit linux system calls.

The code is heavily commented and I tried to keep the comments as up to date as possible, but it is always possible there are mistakes in them.

### There are two main parts to the program:

#### The vm initialisation

This is where the input file is read, the header is parsed (if present) and the heap memory is allocated.
This section is defined in the init\_machine macro and uses a couple of other macros that do smaller tasks.

#### The emulation

This is where the actual emulation of RW instructions happens.
This section starts at the main\_loop label. Here instructions are continuously read from the program counter and memory manipulated accordingly.

How to build
------------
The projecte contains a Makefile, so just running _make_, will be sufficient to build the project. This will build the project with DWARF debug symbols to allow debugging with gdb.

By default there are no boundry checks on memory access within the vm. To enable them run make with the boundry\_check target.
