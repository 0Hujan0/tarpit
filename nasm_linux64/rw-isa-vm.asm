section .data
    ; Messages for errors
    argc_msg db "Incorrect amount of arguments. Please supply exactly one filename.", 0xA
    argc_msg_len equ $ - argc_msg
    file_open_msg db "Unable to open file.", 0xA
    file_open_msg_len equ $ - file_open_msg
    unknown_instruction_msg db "Unknown instruction: 0x__", 0xA
    unknown_instruction_msg_len equ $ - unknown_instruction_msg
    revision_msg db "Revision not supported: _", 0xA
    revision_msg_len equ $ - revision_msg
    bitness_msg db "Pointer size not supported: __ byte", 0xA
    bitness_msg_len equ $ - bitness_msg
    eof_msg db "EOF specified in header does not match file size", 0xA
    eof_msg_len equ $ - eof_msg
    out_of_bounds_msg db "Memory access out of bounds", 0xA
    out_of_bounds_msg_len equ $ - out_of_bounds_msg

    ; Memory for parsing of the header
    header equ $ ; Address of header
    magic_1 db 0 ; magic number 1
    magic_2 db 0 ; magic number 2
    isa_revision db 0 ; revision number
    ptr_size db 0 ; pointer size
    header_len equ $ - header ; length of all the static components of header
    eof_ptr dq 0 ; declare as quadruple word, so we can work with up to 64 bit ptrsize
    eom_ptr dq 0 ; declare as quadruple word, so we can work with up to 64 bit ptrsize

section .text
    global _start

; The actual interpreter code can be found in the _start label further down
; There is also a list of used registers there to assist with readability
; The following macros are mostly used for the loading of the file and setting up of
; the memory. For that, the init_machine macro would be the right place to start reading

; macro for print syscall assumes message in rsi and length in rdx
%macro print 0
    mov rax, 1 ; Write syscall
    mov rdi, 1 ; stdout
    syscall
%endmacro

; Parse command line argument and open specified file. file desciptor will be in r10 after this
; Assumes initial stack setup of program
%macro open_file_from_args 0
    ; Check amount of command line arguments
    pop rax ; argc
    cmp rax, 2
    jne err_argc ; argc != 2

    ; Get filename and issue open syscall
    lea rsp, [rsp + 8] ; ignore argv[0]
    pop rdi ; argv[1]
    mov rax, 2 ; open systemcall
    mov rdx, 0 ; O_RDONLY - open read-only
    mov rsi, 0 ; doesn't matter for read
    syscall
    cmp rax, 0 ; Check return value
    jl err_file_open ; Negative number indicates an error
    mov r10, rax ; store fd
%endmacro

; Get the size of the file associated with the file descriptor in r10
; The size will be in r8 after this
%macro get_file_size 0
    ; Seek all the way to the end to get file size
    mov rax, 8 ; lseek syscall
    mov rdi, r10 ; fd input for syscall
    mov rsi, 0 ; offset = 0
    mov rdx, 2 ; SEEK_END - offset relative to end
    syscall
    mov r8, rax ; store size in r8

    ; Move back to start for later reading
    mov rax, 8 ; lseek syscall
    ;mov rdi, r10 ; fd already in rdi
    ;mov rsi, 0 ; offset already in rsi
    mov rdx, 0 ; SEEK_SET - offset relative to start
    syscall
%endmacro

; Allocate memory according to size in r8
; As far as I am aware, memory is all zero when it is first made accessible to a process
; for security reasons, so we do not need to zero it manually unless we previously
; used it already, which we did not in this case
; Beginning of the allocated memory is stored in r9
%macro alloc 0
    ; Get current program break and store it in r9
    mov rax, 12 ; brk syscall
    mov rdi, 0 ; call with 0 to get current brk
    syscall
    mov r9, rax ; store current break
    ; Increase program break to current break plus allocation size
    lea rdi, [r9 + r8] ; increase break by filesize
    mov rax, 12 ; brk syscall
    syscall
%endmacro

; Read r8 bytes from file associated with the filedescriptor in r10 into buffer r9
; Then close the file
%macro read_from_file 0
    ; Read from file
    mov rax, 0 ; read syscall
    mov rsi, r9 ; read into heap
    mov rdx, r8 ; size of file
    mov rdi, r10 ; fd of open file
    syscall
    ; Close the file
    mov rax, 3 ; close syscall
    ;mov rdi, r10; fd already in rdi
    syscall
%endmacro

; Initialises the machine
; - Parses header, if it exists
; - Allocates memory and copies file into it

; Relevant registers by the end of the macro:
; rbp = Program counter
; r8 = Maximum instruction jmptable offset
; r9 = Start of machine memory
; r10 = fd of (now closed!) file
; r15 = pointer size
%macro init_machine 0
    open_file_from_args ; Reads file descriptor from argv[1] into r10
    get_file_size ; Get file size and store it in r8

    ; If we don't have enough data for a header, it is headerless, i.e. revision 0
    cmp r8, 4
    jle no_header ; If the file has less than 4 bytes, it cannot contain a basic header, so it is header-less
    ; Read all the parts of the header that has static size
    mov rax, 0 ; read syscall
    mov rsi, header ; read into header struct
    mov rdx, header_len ; size of header struct
    mov rdi, r10 ; fd of open file
    syscall

    ; Match magic bytes and parse revision number
    mov al, 'R' ; First magic byte
    cmp al, [magic_1] ; Compare with byte in header
    jne no_header ; If it doesn't match, this is not an RW header
    mov al, 'W' ; Second magic byte
    cmp al, [magic_2] ; Compare with byte in header
    jne no_header ; If it doesn't match, this is not an RW header
    mov al, [isa_revision] ; ISA revision byte
    sub al, 97 ; Get actual revision as number
    mov [isa_revision], al ; Store actual revision number
    cmp al, 2 ; We only implement up to revision 2
    jg err_revision

    ; Get ps value from header and calculate actual pointer size
    mov rcx, 0
    mov cl, [ptr_size] ; log2 of pointer size in ascii number
    sub cl, 48 ; ascii number to number
    mov r15, 1 ; start with 1 so left shift will compute power of two
    shl r15, cl ; pointer size (It has to be CL for shifts, AL wouldn't assemble)

    ; We do not support more than 64
    cmp r15, 8
    jg err_bitness

    ; Read eof pointer using pointer size we now know
    mov rax, 0 ; read syscall
    mov rsi, eof_ptr ; read into eof_ptr
    mov rdx, r15 ; size of ptr_size
    mov rdi, r10 ; fd of open file
    syscall

    ; The end of file pointer should point to what we know is the end of the file
    cmp r8, [eof_ptr] ; check that file size matches eof pointer
    jne err_eof

    ; Read the end of memory pointer
    mov rax, 0 ; read syscall
    mov rsi, eom_ptr ; read into eom_ptr
    mov rdx, r15 ; size of ptr_size
    mov rdi, r10 ; fd of open file
    syscall

    ; Set PC to start of memory
    lea rbp, [header_len + 2 * r15] ; PC starts header size + 2 * ptrsize into memory

    ; Allocate bytes for both the file and the BSS
    mov r8, [eom_ptr] ; Update size to be allocated to include bss
    alloc ; allocate memory based on new value of r8
    mov rbx, [eom_ptr] ; Store eom in rbx for boundry checks

    ; Seek back to beginning of file
    mov rdi, r10 ; use fd of file
    mov rax, 8 ; lseek syscall
    mov rsi, 0 ; offset = 0
    mov rdx, 0 ; SEEK_SET - offset relative to start
    syscall

    ; Read the file into our heap memory
    mov r8, [eof_ptr] ; Set r8 to file size for reading all data
    read_from_file ; Read whole file, since the offsets take header into account

    jmp header_done ; Done with header parsing

    ; We end up here if we have fewer than 4 bytes in the file or we did not
    ; match the RW magic bytes
    no_header:
        ; Seek back to beginning of file, since file pointer potentially moved during header parsing
        mov rdi, r10 ; use fd of file
        mov rax, 8 ; lseek syscall
        mov rsi, 0 ; offset = 0
        mov rdx, 0 ; SEEK_SET - offset relative to start
        syscall

        mov r15, 4 ; Headerless always assumes 4 byte pointer size
        mov rbp, 0 ; Set PC to 0, since there is no header before text segment

        alloc ; allocate heap with file_size (still in r8)
        read_from_file ; read the whole file into the allocated buffer

    header_done:
    ; We are done with r8 for file size and allocation size purposes, so we can now use it for instruction set
    ; We store the max jmptable offset, i.e. 4 for revisions 0 and 1, and 7 for revision 2
    mov r8b, [isa_revision] ; Get the revision number from memory
    cmp r8b, 2 ; Version 2 -> instructions 0 - 7
    jl revision_older ; if less than revision 2, older revision
        mov r8, 7 ; store max instruction offset
        jmp revision_done ; done
    revision_older: ; Version 0 or 1 -> instructions 0 - 4
        mov r8, 4 ; store max instruction offset
    revision_done:
%endmacro

; Get pointer based on pointer size
; Reads pointer at the PC (rbp) location into the given register and advances PC
; appropriately. The register needs to be one of r8 - r15.
; I did have an implementation that would read individual bytes and shift them into
; the register, but that was slower than this simple if - else if structure
%macro get_ptr 1
    %ifdef BOUNDRY_CHECK
        cmp rbp, rbx
        jg err_out_of_bounds
    %endif
    ; We start with the check for 32-bit.
    ; It is the standard and headerless format default to it
    cmp r15, 4 ; 32-bit
    jne %%not_doubleword
    ; double word
        mov %1d, [r9 + rbp] ; Read double word ( Clears upper 32 bit automatically)
        jmp %%end
    %%not_doubleword:
    cmp r15, 1 ; 8-bit
    jne %%not_byte
    ; byte
        mov %1, 0 ; Make sure the whole register is empty
        mov %1b, [r9 + rbp] ; Read byte
        jmp %%end
    %%not_byte:
    cmp r15, 2 ; 16-bit
    jne %%not_word
    ; word
        mov %1, 0 ; Make sure the whole register is empty
        mov %1w, [r9 + rbp] ; Read word
        jmp %%end
    %%not_word:
    ; Only 64-bit left, since we checked earlier that we do not have more
    ; quadruple word
        mov %1, [r9 + rbp] ; Read quadruple word
    %%end:
    lea rbp, [rbp + r15] ; Make sure PC points to after the pointer
%endmacro

; Write pointer based on pointer size
; Writes pointer in the given register at the PC (rbp) location and advances PC
; appropriately. The register needs to be one of r8 - r15.
; I did have an implementation that would write individual bytes and shift them out of
; the register, but that was slower than this simple if - else if structure
%macro write_ptr 1
    %ifdef BOUNDRY_CHECK
        cmp rbp, rbx
        jg err_out_of_bounds
    %endif
    ; We start with the check for 32-bit.
    ; It is the standard and headerless format default to it
    cmp r15, 4 ; 32-bit
    jne %%not_doubleword
    ; double word
        mov [r9 + rbp], %1d ; Write double word
        jmp %%end
    %%not_doubleword:
    cmp r15, 1 ; 8-bit
    jne %%not_byte
    ; byte
        mov [r9 + rbp], %1b ; Write byte
        jmp %%end
    %%not_byte:
    cmp r15, 2 ; 16-bit
    jne %%not_word
    ; word
        mov [r9 + rbp], %1w ; Write Word
        jmp %%end
    %%not_word:
    ; Only 64-bit left, since we checked earlier that we do not have more
    ; quadruple word
        mov [r9 + rbp], %1 ; Write quadruple word
    %%end:
    lea rbp, [rbp + r15] ; Make sure PC points to after the pointer
%endmacro

; reads byte in memory location %1 (needs to be 64 bit register)
; into  %2 (needs to be 8 bit register)
%macro read_byte 2
    %ifdef boundry_check
        cmp %2, rbx
        jg err_out_of_bounds
    %endif

    mov %1, [r9 + %2]
%endmacro

; Write byte in %2 (needs to be 8 bit register)
; into memory location %1 (needs to be 64 bit register)
%macro write_byte 2
    %ifdef BOUNDRY_CHECK
        cmp %1, rbx
        jg err_out_of_bounds
    %endif

    mov BYTE [r9 + %1], %2
%endmacro

; Load pointer to memory location %2 (needs to be 64 bit register)
; into %1 (needs to be 64 bit register)
%macro load_byte_address 2
    %ifdef BOUNDRY_CHECK
        cmp %2, rbx
        jg err_out_of_bounds
    %endif

    lea %1, [r9 + %2]
%endmacro

; reads byte in memory location %1 (needs to be 64 bit register)
; and subtracts it from and into  %2 (needs to be 8 bit register)
%macro read_and_sub_byte 2
    %ifdef boundry_check
        cmp %2, rbx
        jg err_out_of_bounds
    %endif

    sub %1, [r9 + %2]
%endmacro

; Program entry-point
_start:
    init_machine ; Initialise the machine - Parse header if present, else load as headerless

    ; Main loop: Read instruction and execute it

    ; Register usage in main loop:
    ; rax: - used for instruction (al) as jump table offset
    ;      - Specify syscall
    ;      - Used for reading bytes (al)
    ; rbx: Holds eom - Used for boundry check, if enabled
    ; rcx: - !!! Syscalls change this register, use with care !!!
    ;      - Not used in main loop
    ; rdx: Argument for syscalls (length for read and write)
    ; rsi: Argument for syscalls (mem location for read and write)
    ; rdi: Argument for syscalls (file descriptor for read and write)
    ; rsp: - Not used in main loop.
    ;      - (We don't use the stack so it could be used as general purpose)
    ; rbp: Used as program counter
    ; r8 = Maximum instruction jmptable offset
    ; r9 = Start of machine memory
    ; r10 = Temporary storage of PC
    ; r11 = - !!! Syscalls change this register, use with care !!!
    ;       - Not used
    ; r12 = Used to store pointer value
    ; r13 = Used to store pointer value
    ; r14 = Used to store pointer value
    ; r15 = pointer size

    main_loop:
        mov rax, 0 ; We need to clear rax, since we want to use al in a pointer addition
                   ; so we need the whole register
        read_byte al, rbp ; Read byte at PC
        cmp rax, r8 ; Check if revision supports instruction
        jg err_instruction ; If instruction code too high, print error
        inc rbp ; increment PC
        jmp [jmp_table + rax * 8] ; Indirect jump. Read jmp_table + instruction

        ; We use a jump table for the instructions. I think I got slightly better
        ; performance with a simple check for each instruction, but I think that
        ; very much depends on which instructions are most used within a program,
        ; whereas the overhead for this jump table should stay constant, even if new
        ; instructions are added
        jmp_table dq end_success ; Offset 0 = HLT
        jmp_out dq cmd_out ; Offset 1 = OUT
        jmp_bpl dq cmd_bpl ; Offset 2 = BPL
        jmp_sub dq cmd_sub ; Offset 3 = SUB
        jmp_in dq cmd_in ; Offset 4 = IN
        jmp_mov dq cmd_mov ; Offset 5 = MOV
        jmp_beq dq cmd_beq ; Offset 6 = BEQ
        jmp_addp dq cmd_addp ; Offset 7 = ADDP

        ; Instruction OUT = 1
        cmd_out:
            get_ptr r13 ; get ptr to byte to print
            load_byte_address rsi, r13 ; What to print
            mov rdx, 1 ; length of message
            print ; Write syscall macro
            jmp main_loop ; continue
        ; Instruction BPL = 2
        cmd_bpl:
            mov r10, rbp ; save PC for later
            lea rbp, [rbp + r15] ; Skip first pointer, to avoid reading it if we don't jump
            get_ptr r13 ; get ptr to byte to check sign of
            read_byte al, r13 ; read byte
            and al, 10000000b ; Check if signed bit is set
            jnz main_loop ; If bit is set, i.e. negative number -> continue
            mov rbp, r10 ; Back to first pointer
            get_ptr r13 ; Read pointer
            mov rbp, r13 ; Move pointer into PC
            jmp main_loop ; continue
        ; Instruction SUB = 3
        cmd_sub:
            get_ptr r12 ; get dstptr
            get_ptr r13 ; get srcptr
            read_byte al, r12 ; read byte at dstptr
            read_and_sub_byte al, r13 ; substract srcptr from dstptr
            write_byte r12, al ; store byte at dstptr
            jmp main_loop ; continue
        ; Instruction IN = 4
        cmd_in:
            get_ptr r13; get ptr to byte to read to
            load_byte_address rsi, r13 ; Where to read to
            mov rdx, 1 ; length of read
            mov rax, 0 ; Read syscall
            mov rdi, 0 ; stdin
            syscall
            jmp main_loop ; continue

        ; Instruction MOV = 5
        cmd_mov:
            get_ptr r12; get dstptr
            get_ptr r13 ; get srcptr
            read_byte al, r13 ; read byte from srcptr
            write_byte r12, al ; store byte at dstptr
            jmp main_loop ; continue
        ; Instruction BEQ = 6
        cmd_beq:
            mov r10, rbp ; save PC for later
            lea rbp, [rbp + r15] ; Skip first pointer, to avoid reading it if we don't jump
            get_ptr r13 ; get ptr to byte to check sign of
            read_byte al, r13 ; read byte
            cmp al, 0
            jnz main_loop ; If not 0, do not jump to jmpptr
            mov rbp, r10 ; Back to first pointer
            get_ptr r13 ; Read pointer
            mov rbp, r13 ; Move pointer into PC
            jmp main_loop ; continue
        ; Instruction ADDP = 7
        cmd_addp:
            get_ptr r12 ; get dstptr
            get_ptr r13 ; Read srcptr
            mov r10, rbp ; back up current PC
            mov rbp, r13 ; Move srcptr into PC

            get_ptr r14 ; Get ptr value from srcptr
            mov rbp, r12 ; Set PC to dstptr to read ptr value
            get_ptr r13 ; Get ptr value from dstptr
            add r13, r14 ; Add the two pointers
            mov rbp, r12 ; Set PC to dstptr to write ptr value
            write_ptr r13 ; write pointer to dstptr

            mov rbp, r10 ; Restore PC
            jmp main_loop ; continue

; End successfully. Called by HLT command
end_success:
    mov rax, 60 ; Exit syscall
    mov rdi, 0 ; Return code
    syscall

; Error in the number of command line arguments
err_argc:
    mov rsi, argc_msg ; Load message
    mov rdx, argc_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 1 ; Return code
    syscall

; Error opening the file
err_file_open:
    mov rsi, file_open_msg ; Load message
    mov rdx, file_open_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 2 ; Return code
    syscall

; Unknown instruction
err_instruction:
    ; We want to print the instruction in hexadecimal and inject it into the message
    ; We first calculate the ASCII for the two hex digits of the instruction and then
    ; write them directly into the message at the appropriate offset
    mov cx, 16 ; Set devisor
    mov dx, 0 ; set remainder to 0 before division
    idiv cx ; al / cl - remainder: dl
    add al, 48 ; Map quotient to ascii
    add dl, 48 ; Map remainder to  ascii

    cmp al, 58 ; Higher than number range?
    jl al_ok ; if in number range, do nothing
        add al, 7 ; Map 10-15 to A-F ascii
    al_ok:
    cmp dl, 58 ; Higher than number range?
    jl dl_ok ; if in number range, do nothing
        add dl, 7 ; Map 10-15 to A-F ascii
    dl_ok:

    lea rcx, [unknown_instruction_msg + unknown_instruction_msg_len] ; end of message
    sub rcx, 2 ; Skip newline and go to lower digit placeholder
    mov [rcx], dl ; Write lower digit into message
    dec rcx ; higher digit placeholder
    mov [rcx], al ; Write higher digit into message
    mov rsi, unknown_instruction_msg ; Load message
    mov rdx, unknown_instruction_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 3 ; Return code
    syscall

; Unsupported revision number
err_revision:
    add al, 48 ; Map revision to ascii number
    lea rcx, [revision_msg + revision_msg_len] ; end of message
    sub rcx, 2 ; the newline and revision placeholder
    mov [rcx], al ; Write digit into message
    mov rsi, revision_msg ; Load message
    mov rdx, revision_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 4 ; Return code
    syscall

; Unsupported pointer size
err_bitness:
    ; We want to print the pointer size in decimal and inject it into the message
    ; We first calculate the ASCII for the two decimal digits of the pointer size and then
    ; write them directly into the message at the appropriate offset
    mov al, r15b ; Get pointer size
    mov cx, 10 ; Set devisor
    mov dx, 0 ; set remainder to 0 before division
    idiv cx ; al / cl - remainder: dl
    add al, 48 ; Map quotient to ascii
    add dl, 48 ; Map remainder to ascii

    lea rcx, [bitness_msg + bitness_msg_len] ; end of message
    sub rcx, 7 ; Go to lower digit placeholder
    mov [rcx], dl
    dec rcx ; higher digit placeholder
    mov [rcx], al
    mov rsi, bitness_msg ; Load message
    mov rdx, bitness_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 5 ; Return code
    syscall

; The eof pointer in the header does not match the actual end of file
err_eof:
    mov rsi, eof_msg ; Load message
    mov rdx, eof_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 6 ; Return code
    syscall

err_out_of_bounds:
    mov rsi, out_of_bounds_msg ; Load message
    mov rdx, out_of_bounds_msg_len ; Load message length
    print
    mov rax, 60 ; Exit syscall
    mov rdi, 7 ; Return code
    syscall
